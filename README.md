## Install packages under Camping_planner folder
```Bash
npm install
```

## Setup .env file according to .env.sample

## Database set up

1. create database
2. run table.sql to create tables
3. run seed files 

```Bash
npx ts-node insert_data.ts
```
## Run Camping_planner folder

```Bash
npm start
```