CREATE DATABASE camping_planner;

\c camping_planner;

CREATE TABLE users(
    id SERIAL PRIMARY KEY ,
    username VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    password TEXT NOT NULL,
    experience TEXT NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    event_id INT
);

CREATE TABLE facilities(
    id SERIAL PRIMARY KEY,
    name TEXT,
    is_hygienic boolean
);

CREATE TABLE campsite(
    id SERIAL PRIMARY KEY,
    campsite_name TEXT,
    location TEXT,
    campsite_category TEXT,
    accessibility TEXT,
    description TEXT,
    facility_id INTEGER,
    FOREIGN KEY (facility_id) REFERENCES facilities(id),
    source_of_water TEXT,
    highlights TEXT,
    how_to_get_there TEXT,
    remarks TEXT,
    image TEXT,
    latitude FLOAT,
    longitude FLOAT
);

CREATE TABLE food_recommendation(
    id SERIAL PRIMARY KEY,
    image TEXT,
    description TEXT,
    difficulty TEXT,
    step TEXT,
    ingredient TEXT,
    seasoning TEXT
);

CREATE TABLE event_detail(
    id SERIAL PRIMARY KEY,
    campsite_id INTEGER,
    FOREIGN KEY (campsite_id) REFERENCES campsite(id),
    start_date Date,
    end_date Date,
    weather Text,
    checklist_id int,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);

CREATE TABLE equipment_category(
    id SERIAL PRIMARY KEY,
    category_name TEXT
);

CREATE TABLE equipment_items(
    id SERIAL PRIMARY KEY,
    name TEXT,
    category_id INTEGER,
    FOREIGN KEY (category_id) REFERENCES equipment_category(id)
);

-- column activity_equipment rename to equipment_id 
CREATE TABLE default_activities(
    id SERIAL PRIMARY KEY,
    remark TEXT,
    activity_name TEXT,
    equipment_id INTEGER,
    FOREIGN KEY (equipment_id) REFERENCES equipment_items(id)
);



CREATE TABLE user_joining_event(
    id SERIAL PRIMARY KEY,
    users_id INTEGER,
    FOREIGN KEY (users_id) REFERENCES users(id),
    event_id INTEGER,
    FOREIGN KEY (event_id) REFERENCES event_detail(id)
);

-- ERD change data type for is_default
CREATE TABLE users_create_activities(
    id SERIAL PRIMARY KEY,
    start_date DATE,
    end_date DATE,
    start_time TIME NOT NULL,
    end_time TIME NOT NULL,
    activity_name TEXT,
    remark TEXT,
    event_id INTEGER,
    FOREIGN KEY (event_id) REFERENCES event_detail(id),
    default_id INTEGER,
    FOREIGN KEY (default_id) REFERENCES default_activities(id),
    is_default BOOLEAN 
);

-- change relationship of enent_id and table event_detail
CREATE TABLE equipment_joining_events(
    id SERIAL PRIMARY KEY,
    equipment_id INTEGER,
    FOREIGN KEY (equipment_id) REFERENCES equipment_items(id),
    event_id INTEGER,
    FOREIGN KEY (event_id) REFERENCES event_detail(id),
    remark TEXT,
    is_prepared BOOLEAN
);

-- delete not null in users experience column
alter table users alter experience drop not null;

-- insert dummy data for testing
insert into users (username, email, password) values ('edwin','edwinkwantc@gmail.com','123')

CREATE TABLE event_filtering(
    id SERIAL PRIMARY KEY,
    number_of_participants INTEGER,
    experience Text,
    start_date Date,
    end_date Date,
    campsite_name Text
);
