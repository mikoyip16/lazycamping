import { Request, Response, NextFunction } from "express";
import path from "path";

export const isLoggedIn = (req: Request, res: Response, next: NextFunction) => {
    const user =req.session["user"];
    if(!user) {
        console.log("not yet login");
        res.sendFile(path.join(__dirname, "./public/html/index.html"));
    }else{
        next();
    }
};