import express from "express";
import { client } from "../main";
export const campsiteRoutes = express.Router();


//Step Two: pass all campsite location to 請選擇你的營地地點
campsiteRoutes.get("/", async (req, res) => {
    try {
        const result = (await client.query('SELECT * FROM campsite')).rows
        res.status(200).json(result)

    } catch (err) {
        console.error(err.message)
        res.status(500).json({ message: "internal server error" })
    }
})

//For editEvent: pass campsite Information base on campsiteId [editEvent.js function displayCampsiteInformation(campsiteId)]
campsiteRoutes.get("/:campsiteId", async (req, res) => {
    try{

        const campsiteId = req.params.campsiteId;
        console.log("campsiteId" + campsiteId)
        const campsiteInfo = (await client.query('SELECT * FROM campsite WHERE id = $1',[
            parseInt(campsiteId),
        ])).rows       
        res.status(200).json(campsiteInfo[0])

    }catch(err){
        console.error(err.message)
        res.status(500).json({message:"Cannot get campsite information, internal server error"})
    }
})

//
campsiteRoutes.get("/filter/:userExperience", async(req, res) => {
    try{
        const userExperience = req.params.userExperience;
        
        if ( userExperience === 'beginner'){
            const result = (await client.query('SELECT id,campsite_name,image,description From campsite WHERE accessibility = $1',['初次體驗露營人士'])).rows
            res.status(200).json(result)
        
        }else if( userExperience === 'intermediate' ){
            const result = (await client.query('SELECT id,campsite_name,image,description From campsite WHERE accessibility = $1',['有經驗之遠足及露營人士'])).rows
            res.status(200).json(result)
        }
        else{
            console.log(userExperience)
            res.status(200).json({message:"cannot find"})
        }

    }catch(err){
        console.error(err.message)
        res.status(500).json({message:"internal server error"})
    }
});