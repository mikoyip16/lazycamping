import express,{Request,Response} from 'express'
import { client } from '../main'

export const eventManRoutes = express.Router()

eventManRoutes.get('/',async(req,res) => {
    try{
        const eventDetails = (await client.query('SELECT * FROM event_detail')).rows
        res.status(200).json({ eventDetails })
        console.log(eventDetails)
    } catch (err) {
        console.error(err)
        res.status(500).json({message:"internal server error"})
    }
})

eventManRoutes.get('/joinEvent/:users_id',async(req:Request,res:Response) => {
    try{
        const { users_id } = req.params;
        const userJoiningEvent = (await client.query('SELECT * FROM user_joining_event where users_id = $1',[users_id])).rows
        res.status(200).json({ userJoiningEvent })
        console.log(userJoiningEvent)
    } catch (err) {
        console.error(err)
        res.status(500).json({message:"internal server error"})
    }
})


eventManRoutes.put('/updateEventId', async(req, res) => {
    try{
        req.session["eventID"] = req.body.eventId;              
        res.json({message:"Success"}) 
        console.log(req.session["eventID"])             
        
    }catch(err){
        console.error(err.message)
        res.status(500).json({message:"internal server error"})
    }
})


eventManRoutes.delete('/updateEventId/:eventId', async(req, res) => {
    console.log(req.params.eventId)
    
    try{
       const eventId = parseInt(req.params.eventId)
       const result = await client.query('delete from user_joining_event where event_id = $1',[eventId]) ;
       if(result.rowCount === 0) {
           res.status(400).json({ message: "event not found"});
    
    }           
        res.json({message:"Success"}) 
        console.log(req.session["eventID"])  
    }catch(err){
        console.error(err.message)
        res.status(500).json({message:"internal server error"})
    }
})
















