import express from "express";
import { client } from "../main";
export const activityRoutes = express.Router();

activityRoutes.get("/defaultActivity", async (req, res) => {
    try {
        const result = (await client.query('SELECT * FROM default_activities')).rows
        res.status(200).json(result)

    } catch (err) {
        console.error(err.message)
        res.status(500).json({ message: "Internal server error" })
    }
})

//create new activity
activityRoutes.post("/", async (req, res) => {
    try {
       

        const { startDate,                 
                startTime, 
                endTime,
                activityName,
                remark } = req.body;
        const eventId = req.session["eventID"];
        await client.query('INSERT INTO users_create_activities (start_date, start_time, end_time, activity_name, remark, event_id) VALUES ($1, $2, $3, $4, $5, $6)',[
            startDate,            
            startTime,
            endTime,
            activityName,
            remark,
            eventId
        ])  
        
        res.json({message:"ActivityRoute Create activity Success"})



    }catch(err){
        console.error(err.message)
        res.status(500).json({message:"Internal server error"})
    }

    
})


//pass activity data to event edit page by event id
activityRoutes.get("/:eventId", async (req, res) => {

    try{

        const eventID = req.params.eventId;
        
        console.log('[activityRoutes.get]: Getting activity data by  ' + eventID)
        const activityInfo = (await client.query('SELECT * FROM users_create_activities WHERE event_id = $1',[
            parseInt(eventID),
        ])).rows
        console.log('[activityRoutes.get]: Getting activityInfo  ' + activityInfo)
        res.status(200).json(activityInfo)

    }catch(err){
        console.error(err.message)
        res.status(500).json({message:"Cannot get event information, internal server error"})
    }
})

// delete activity data to event edit page by event id
activityRoutes.delete("/:activityId", async (req,res) => {
    
    try{
        const activityId = req.params.activityId

        const deleteAct = await client.query('DELETE FROM users_create_activities WHERE id = $1',[
            parseInt(activityId)
        ])
        console.log('[activityRoutes.delete]: Getting activityInfo' + deleteAct)
        res.status(200).json({message:"delete success"})
    } catch (err) {
        console.error(err.message)
        res.status(500).json({message:"Internal server error"})
    }
})

// update activity data to event edit page by event id
activityRoutes.put("/:activityId", async (req, res) => {
    console.log('byebye world')
    console.log(req.params.activityId)
    console.log(req.body.activityName)

    
    try{
        const activityId = parseInt(req.params.activityId)
        const { activityName } = req.body
        if(isNaN(activityId)){
            res.status(400).json({message: "Invalid activity id"})
            return
        }

        if(!activityName){
            res.status(400).json({message: "Invalid input"})
            return
        }
        
        const result = await client.query("UPDATE users_create_activities set activity_name = $1 where id = $2",[
            activityName,
            activityId
        ])

        if(result.rowCount === 0){
            res.status(400).json({message: "Activity not found"})
            return
        }
        res.json({message: "Success"})
    } catch(err) {
        console.error(err.message)
        res.status(500).json({message:"Internal server error"})
    }
})

