import express from "express";
import { client } from "../main";
export const eventRoutes = express.Router();

//For Step three: create new event [step3.js function stepThreeCreateNewEvents()]
eventRoutes.post("/", async(req, res) => {
    try{
        const eventLocation = req.body.location;
        const eventStartDate = req.body.startDate;
        const eventEndDate = req.body.endDate;
        const userId = req.body.userId;
        const campsiteId = (await client.query("SELECT id FROM campsite WHERE campsite_name = $1", [eventLocation])).rows
        console.log('[eventRoutes.post(/): create New Event [CampsiteId]] '  + campsiteId);
        const eventId = (await client.query('INSERT INTO event_detail (campsite_id, start_date, end_date) VALUES ($1, $2, $3) returning id',[
            campsiteId[0].id,
            eventStartDate,
            eventEndDate
        ])).rows
        
        console.log('[eventRoutes.post(/): create New Event [eventId]] ' + eventId)
        await client.query('INSERT INTO user_joining_event (users_id, event_id) VALUES ($1, $2)', [
            userId,
            eventId[0].id
        ])
        
        console.log('[eventRoutes.post(/): create New Event [userId] + [sessionUserId] + [eventId]' +  userId + ' + ' + req.session["user"]["userId"] + ' + ' + eventId)
        req.session["eventID"] = eventId[0].id
        res.json({message:"Create Event Success"})


    }catch(err){
        console.error(err.message)
        res.status(500).json({message:"internal server error"})
    }
    
});

//For editEvent: pass eventId 
eventRoutes.get("/default", async(req, res) => {
    try{
        console.log('[eventRoutes.get(/default)]: session[eventID] ' + req.session["eventID"])
        res.status(200).json(req.session["eventID"])
        

    }catch(err){
        console.error(err.message)
        res.status(500).json({message:"internal server error"})
    }
})


//For editEvent: pass event detail information to editEventPage []
eventRoutes.get("/:id", async(req, res) => {

    try{

        const eventID = req.params.id
        
        console.log('[eventRoutes.get]: Getting eventInformation by  ' + eventID)
        const eventInfo = (await client.query('SELECT * FROM event_detail WHERE id = $1',[
            parseInt(eventID),
        ])).rows
        console.log('[eventRoutes.get]: Getting eventInfo  ' + eventInfo[0])
        res.status(200).json(eventInfo[0])

    }catch(err){
        console.error(err.message)
        res.status(500).json({message:"Cannot get event information, internal server error"})
    }
})