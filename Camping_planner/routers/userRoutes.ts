import express from 'express'
import { client } from '../main'
import { User } from '../database_model'

export const userRoutes = express.Router()

userRoutes.post('/login', async (req, res) => {
    const { email, password } = req.body;
    if (!email || !password) {
        res.status(400).json({ message: 'missing email/password' })
        return;
    }
    const users: User[] = (await client.query('SELECT * FROM users where email = $1 and password = $2', [email, password])).rows;
    const foundUser = users[0];

    if (!foundUser) {
        res.status(400).json({ message: 'invalid email/password' })
        return;
    }
    req.session["user"] ={
        username: foundUser.username,
        userId:foundUser['id']
    };
    res.status(200).json({message:'success'})
})


//submit new user account to database
userRoutes.post('/signUp', async (req, res) => {
    const { username, email, password } = req.body
    const ids = (await client.query('INSERT INTO users (username, email, password) values ($1, $2, $3 ) returning id',[
        username,
        email,
        password])).rows
    res.json({message: `Success with ${ids}`})
}) 


//find userInformation
userRoutes.get('/getUserInformation', async(req,res) => {
    try{
        console.log(req.session["user"])
        res.status(200).json(req.session["user"])
        

    }catch(err){
        console.error(err.message)
        res.status(500).json({message:"internal server error"})
    }
})

