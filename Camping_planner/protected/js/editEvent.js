
const menuButton = document.getElementById("add")
const mobileMenu = document.getElementById("drop")
const dropdownContent = document.querySelector('.dropdownContent')

menuButton.addEventListener('click', function () {

    mobileMenu.classList.toggle('show')

})



// tab page
const selectName1 = document.querySelector("#default")
const showContent1 = document.querySelector("#schedule")

const selectName2 = document.querySelector("#campsite")
const showContent2 = document.querySelector("#campsiteContent")

const selectName3 = document.querySelector("#checklist")
const showContent3 = document.querySelector("#checklistContent")

const selectName4 = document.querySelector("#weatherTraffic")
const showContent4 = document.querySelector("#weatherTrafficContent")

selectName1.addEventListener("click", (event) => {
  if (showContent1.style.display = "none") {
    showContent1.style.display = "block";
    showContent2.style.display = "none";
    showContent3.style.display = "none";
    showContent4.style.display = "none";
  }
})

selectName2.addEventListener("click", (event) => {
  
  if (showContent2.style.display = "none") {
 
    showContent2.style.display = "block";
    showContent1.style.display = "none"
    showContent3.style.display = "none"
    showContent4.style.display = "none"
  }
})
selectName3.addEventListener("click", (event) => {
  if (showContent3.style.display = "none") {
    showContent3.style.display = "block";
    showContent1.style.display = "none"
    showContent2.style.display = "none"
    showContent4.style.display = "none"
  }
})
selectName4.addEventListener("click", (event) => {
  if (showContent4.style.display = "none") {
    showContent4.style.display = "block";
    showContent1.style.display = "none"
    showContent2.style.display = "none"
    showContent3.style.display = "none"
  }
})

document.getElementById("default").click();


// add default activity button
const selectCustoms = document.querySelector("#activities")
const showinput = document.querySelector(".customInput")

selectCustoms.addEventListener('change', (event) => {
  if (selectCustoms.value == 'custom') {
    showinput.style.display = "block";
  } else {
    showinput.style.display = "none";
  }
})

// checklist 

// close button
const myNodelist = document.querySelectorAll(".my > li");
for (const li of myNodelist) {
  for (let i = 0; i < myNodelist.length; i++) {
    const span = document.createElement("addItems");
    const txt = document.createTextNode("X");
    span.className = "close";
    span.appendChild(txt);
    myNodelist[i].appendChild(span);
  }
}


// close button to hide
const close = document.getElementsByClassName("close");
for (let i = 0; i < close.length; i++) {
  close[i].onclick = function () {
    const div = this.parentElement;
    div.style.display = "none";
  }
}

// change style to "checked"
const list = document.querySelectorAll(".my > li");
for (const li of list) {
  li.addEventListener("click", function (event) {
    event.target.classList.toggle("checked");
  });
}

// add button to add new item
function newElement() {
  const li = document.createElement("li");
  const inputValue = document.getElementById("checklistInput").value;
  const texted = document.createTextNode(inputValue);
  li.appendChild(texted);
  li.addEventListener("click", function (event) {
    event.target.classList.toggle("checked");
  });
  if (inputValue === '') {
    alert("You must write something!");
  } else {
    document.getElementById("myList").appendChild(li);
  }
  document.getElementById("checklistInput").value = "";

  const span = document.createElement("SPAN");
  const txt = document.createTextNode("X");
  span.className = "close";
  span.appendChild(txt);
  li.appendChild(span);


  for (i = 0; i < close.length; i++) {
    close[i].onclick = function () {
      const div = this.parentElement;
      div.style.display = "none";

    }
  }

}




main();

async function main() {
  await displayTimeLine(); 
  const eventId = await getEventID();  
  const campsiteId = (await getEventInformation(eventId))["campsite_id"];
  await displayCampsiteInformation(campsiteId);
  await displayDefaultActivity();
  await createActivity();




}


//get eventID routers: eventRoutes
async function getEventID() {
  const res = await fetch('/events/default')
  const eventId = await res.json();
  return eventId;

}

//get event information routers: eventRoutes
async function getEventInformation(eventId) {
  const res = await fetch(`/events/${eventId}`)
  const eventData = await res.json()
  return eventData;
}



//display 營地資料 and 交通 in event edit page
async function displayCampsiteInformation(campsiteId) {
  const res = await fetch(`/campsite/${campsiteId}`)
  const campsiteInfo = await res.json();
  const campsiteName = document.getElementById("campsite_Name");
  const campsiteDescription = document.getElementById("campsite_description");
  const transportation = document.getElementById("transportation")
  const howToGetThere = document.getElementById("traffic")
  const campImg = document.getElementById("campImg")
  const ramPic = Math.floor(Math.random()*8);
  console.log(ramPic)
  campImg.innerHTML=`
  <img src=../images/${ramPic}.jpg width="100%">
  `
  campsiteName.innerHTML = `
  <p>SEE&nbsp &nbsp & &nbsp &nbspEXPLORE</p>
  <h1 class="camp_name">${campsiteInfo["campsite_name"]}</h1>
  `
  campsiteDescription.innerHTML = `
  <div class="info-box">
  <div class="info-title">地點: </div>
  <div class="info-content">${campsiteInfo["location"]}</div>
</div>
<div class="info-box">
  <div class="info-title">營地大小：</div>
  <div class="info-content">${campsiteInfo["campsite_category"]}</div>
</div>
<div class="info-box">
  <div class="info-title">適合人士： </div>
  <div class="info-content">${campsiteInfo["accessibility"]}</div>
</div>
<div class="info-box">
  <div class="info-title">簡介：</div>
  <div class="info-content">${campsiteInfo["description"]}</div>
</div>
<div class="info-box">
  <div class="info-title">水源：</div>
  <div class="info-content">${campsiteInfo["source_of_water"]}</div>
</div>
<div class="info-box">
  <div class="info-title">營地特色：</div>
  <div class="info-content">${campsiteInfo["highlights"]}</div>
</div>
<div class="info-box">
  <div class="info-title">備註：</div>
  <div class="info-content">${campsiteInfo["remarks"]}</div>
</div>
   `

  howToGetThere.innerHTML = `
   <div class="info-box">
      <div class="info-title">交通: </div>
      <div class="info-content">
      ${campsiteInfo["how_to_get_there"]}
      </div>
    </div>
   `
}



//display default_activity info in 加入活動
async function displayDefaultActivity() {
  const res = await fetch(`/activity/defaultActivity`)
  const defaultActivityData = await res.json();
  const defaultActivityOption = document.getElementById("activities")
  let activityHtmlStr = '<option selected="selected">選擇活動</option>';
  for (let activity of defaultActivityData) {
    const activityOption = activity["activity_name"];
    activityHtmlStr += `
    <option value="${activityOption}">${activityOption}</option>
    `
  }
  activityHtmlStr += '<option value="custom" class="custom">自訂</option>'
  defaultActivityOption.innerHTML = activityHtmlStr;
}




//create activity

async function createActivity() {
  document.getElementById("addNewActivity").addEventListener("click", async function (e) {
    e.preventDefault();
    let newActivityName = "default";
    const selectedActivity = document.getElementById("activities").value;
    const customActivity = document.getElementById("input").value

    const eventId = await getEventID();
    const eventStartDate = (await getEventInformation(eventId))["start_date"];
    const eventEndDate = (await getEventInformation(eventId))["end_date"];

    const eventStartDT = new Date(eventStartDate)
    const eventEndDT = new Date(eventEndDate)
  
    let selectStartDate = new Date(document.getElementById("activity-start-date").value)

    let startTime = document.getElementById("activity-start-time").value;
    let endTime = document.getElementById("activity-end-time").value;

    

    // let date1 = new Date()
    eventEndDT.setDate(eventEndDT.getDate() + 1)

    if (selectedActivity === "custom" && customActivity !== "") {
      newActivityName = customActivity;
    } else if (selectedActivity !== "custom") {
      newActivityName = selectedActivity;
    }
    if (newActivityName === "選擇活動" || newActivityName === "default") {
      alert("請輸入活動")
    } 


    // validation of activity date
    if (selectStartDate.getTime() < eventStartDT.getTime()
      || selectStartDate.getTime() > eventEndDT.getTime()){
      alert("入錯日期")
      return

    } else {
      startDate = document.getElementById("activity-start-date").value;
      startTime = document.getElementById("activity-start-time").value;
      endTime = document.getElementById("activity-end-time").value;
      const activityName = newActivityName;
      const remark = document.getElementById("remark").value;
      const activityForm = { startDate, startTime, endTime, activityName, remark }
      document.querySelector('.dropBox').reset()
      const res = await fetch('/activity', {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(activityForm)
      })
      if (res.status === 200) {
        console.log("success")
        dropdownContent.classList.toggle('show')
      } else {
        const data = await res.json();
        const errMessage = data.message;
        alert(errMessage)
      }
    }
    await displayTimeLine();
  })
}

// Delete Activity


async function deletion(id) {

  console.log(id)

  try {
    const res = await fetch(`/activity/${id}`, {
      method: "DELETE"
    })
    if (res.status === 200) {
      await displayTimeLine();
    }
  } catch (errMessage) {
    alert(errMessage)
  }
}
//

async function date_diff(date1, date2) {
  dt1 = new Date(date1);
  dt2 = new Date(date2);
  return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
}


// display Time Line

async function displayTimeLine() {
  const timeLine = document.querySelector(".main-timeline")

  const eventId = await getEventID();
  const eventStartDate = (await getEventInformation(eventId))["start_date"];
  const copyStartDate = eventStartDate
  const eventEndDate = (await getEventInformation(eventId))["end_date"];
  const days = await date_diff(eventStartDate, eventEndDate)
  const date = new Date(copyStartDate);
  const startYear = new Date(eventStartDate).getFullYear();
  const startMonth = new Date(eventStartDate).getMonth() + 1;
  const startDate = new Date(eventStartDate).getDate();
  const startMemo = await newDisplayAllActivity(eventId, startDate);
  let timeHtml = `
  <div class="timeline">
      <div class="icon"></div>
        <div class="date-content">
            <div class="date-outer">
              <span class="date">
                <span class="month">${startDate} / ${startMonth}</span>
                <span class="year">${startYear}</span>
              </span>
            </div>
        </div>
      <div class="timeline-content">
          <div class="try-activity" id="date-${startDate}">
          ${startMemo}
          </div>
      </div>
  </div>
  `

  for (let i = 0; i < days; i++) {


    date.setDate(date.getDate() + 1)
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const dt = date.getDate();
    const memo = await newDisplayAllActivity(eventId, dt);
    timeHtml += `
    <div class="timeline">
        <div class="icon"></div>
          <div class="date-content">
              <div class="date-outer">
                <span class="date">
                  <span class="month">${dt} / ${month}</span>
                  <span class="year">${year}</span>
                </span>
              </div>
          </div>
        <div class="timeline-content">
            <div class="try-activity" id="date-${dt}">
            ${memo}
            </div>
        </div>
    </div>
    `

  }
  timeLine.innerHTML = timeHtml;

}

function textboxShow() {
  const nameBoxs = document.querySelectorAll('.nameBox')
  console.log(nameBoxs)
  for (const nameBox of nameBoxs) {
    nameBox.classList.toggle('inactive')
  }

}


async function updateAct(id) {
  console.log(id)

  const form = this;
  const activityName = form["activityName"].value
  const formObject = { activityName }

  console.log(formObject)

  try {
    const res = await fetch(`/activity/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json, charset=utf-8"
      },
      body: JSON.stringify(formObject)
    })
    if (res.status === 200) {
      const eventId = await getEventID();
      await displayAllActivity(eventId)
    }
  } catch (errMessage) {
    alert(errMessage)
  }
}



//New display activity in activity board
async function newDisplayAllActivity(eventId, startDate) {
  const res = await fetch(`/activity/${eventId}`);
  const activityData = await res.json();
  let activityBoardHtmlStr = '';
  for (let i = 0; i < activityData.length; i++) {
    const check = new Date(activityData[i]["start_date"]).getDate();
    
    if (check === startDate) {
      activityBoardHtmlStr += `
      <div class="activity-memo">
          <div class="activity-title">活動：${activityData[i]["activity_name"]}</div>
          <div class="space">
              <div class="activity-time">開始時間：${activityData[i]["start_time"]}</div>
              <div class="activity-time">結束時間：${activityData[i]["end_time"]}</div>            
          </div>
          <div class="activity-time">活動簡介：${activityData[i]["remark"]}</div>
          <button onclick=deletion(${activityData[i]["id"]}) class="deleteActivity" id="${activityData[i]["id"]}">DELETE</button>
      </div>  
      `
    }
  }
  return activityBoardHtmlStr;
}




$("#activity-start-time").change(function () {
  const startTime = document.getElementById("activity-start-time").value;
  const endTimeMin = document.getElementById("activity-end-time");
  endTimeMin.min = startTime;


});


const weatherForecast = document.querySelector('.weatherForecast')

weatherForecast.addEventListener('click', function (e) {
  e.preventDefault()
  window.location.assign('http://localhost:8080/html/ProtectedweatherForecast.html')
})

const eventManagementPage = document.querySelector('.eventMan')

eventManagementPage.addEventListener('click', function (e) {
  e.preventDefault()
  window.location.assign('http://localhost:8080/html/eventMan.html')
})

const createEvent = document.querySelector('.createEvent')

createEvent.addEventListener('click', function (e) {
  e.preventDefault()
  window.location.assign('http://localhost:8080/html/stepper.html')
})




