main();

async function main() {
  await createUserEventCard();
  helloUser();
}

const welcomeWordings = document.querySelector(".bg-text");

async function helloUser() {
  const res = await fetch("/getUserInformation");
  const userData = await res.json();
  users_id = userData.userId;
  welcomeWordings.innerHTML = `<h1>Welcome back,</h1>
    <h2> ${userData.username} !</h2>`;
  await getAllUserEvent(users_id);
}

async function createUserEventCard() {
  console.log(123);
  const drawUserEventCard = document.querySelector(".userEventCard");
  const userId = await getUserID();
  const eventId = await getAllUserEvent(userId);
  let htmlStr = ``;
  let twoArray = [6];
  twoArray.push(eventId.length);
  const reducer = (accumulator, currentValue) =>
    accumulator < currentValue ? accumulator : currentValue;
  let smallestLength = twoArray.reduce(reducer);
  for (let i = 0; i < smallestLength; i++) {
    const eventInfo = await getEventInformation(eventId[i]["event_id"]);
    const campsiteId = eventInfo["campsite_id"];
    const campsiteName = await getCampsiteInformation(campsiteId);
    console.log(campsiteId, campsiteName);

    htmlStr += `
        <div class="card">        
        <div class="card-body">
        <div class="card-title">地點： ${campsiteName}</div>
        <div class="card-text">開始日期： ${moment(
          eventInfo["start_date"]
        ).format("ll")}</div>
        <div class="card-text">結束日期： ${moment(
          eventInfo["end_date"]
        ).format("ll")}</div>
        <div class="twoButton">
        <div class="editEvent" onclick="updateSessionEventId(${
          eventId[i]["event_id"]
        })">編輯</div>
        <div class="deleteButton" onclick="confirmDeleteFunction(${
          eventId[i]["event_id"]
        })">刪除</div>
        </div>
        </div>
        </div>        
        `;
  }
  console.log(eventId)
  if (eventId.length == 0) {
    drawUserEventCard.innerHTML = `                    
    <div class="card">
    <div class="card-body">
        <div class="card-title">Opps! 暫時未有露營計劃</div>
    </div>
    </div>`;
  } else {
    drawUserEventCard.innerHTML = htmlStr
  }
}


async function getUserID() {
  const res = await fetch("/getUserInformation");
  const userInformation = await res.json();

  return userInformation["userId"];
}

async function getAllUserEvent(users_id) {
  const formObject = { users_id };
  const res = await fetch(`/eventManagement/joinEvent/${users_id}`);
  const joinEventData = await res.json();
  return joinEventData["userJoiningEvent"];
}

//Get eventInfo
async function getEventInformation(eventId) {
  const res = await fetch(`/events/${eventId}`);
  const eventData = await res.json();
  console.log(eventData);
  return eventData;
}

//Get CampsiteInfo
async function getCampsiteInformation(campsiteId) {
  const res = await fetch(`/campsite/${campsiteId}`);
  const campsiteInfo = await res.json();
  return campsiteInfo["campsite_name"];
}

//Select event and redirect to editEventPage
async function updateSessionEventId(eventId) {
  console.log(eventId);

  const editEventForm = { eventId };
  const res = await fetch("/eventManagement/updateEventId", {
    method: "PUT",
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    body: JSON.stringify(editEventForm),
  });
  if (res.status === 200) {
    window.location.assign("http://localhost:8080/html/editEvent.html");
  } else {
    const data = await res.json();
    const errMessage = data.message;
    alert(errMessage);
  }
}



const confirmDeleteCard = document.querySelector(".confirmDeleteCard");
const confirmDelete = document.querySelector("#confirmDelete");
const notDelete = document.querySelector("#notDelete");

function confirmDeleteFunction(eventId) {
  let eventID = eventId;
  let confirmDeleteValue = 0;
  confirmDeleteCard.classList.remove("inactive");
  confirmDelete.addEventListener("click", async function (e) {
    e.preventDefault();
    confirmDeleteValue = 1;
    console.log(confirmDeleteValue);
    confirmDeleteCard.classList.add("inactive");
    const res = await fetch(`/eventManagement/updateEventId/${eventID}`, {
      method: "DELETE",
    });
    await createUserEventCard();
  });
  notDelete.addEventListener("click", function (e) {
    e.preventDefault();
    confirmDeleteValue = 0;
    console.log(confirmDeleteValue);
    confirmDeleteCard.classList.add("inactive");
  });

  if ((confirmDeleteValue = 1)) {
    return true;
  }
}


const weatherForecast = document.querySelector(".weatherForecast");

weatherForecast.addEventListener("click", function (e) {
  e.preventDefault();
  window.location.assign(
    "http://localhost:8080/html/ProtectedweatherForecast.html"
  );
});

const createEvent = document.querySelector(".createEvent");

createEvent.addEventListener("click", function (e) {
  e.preventDefault();
  window.location.assign("http://localhost:8080/html/stepper.html");
});

