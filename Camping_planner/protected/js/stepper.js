let stepOneParticipant;
let stepOneExperience;
let stepOneStartDate;
let stepOneEndDate;

const stepOneTag = document.querySelector("#step-one-tag");
const stepOneContent = document.querySelector("#step-one-content");

const stepTwoTag = document.querySelector("#step-two-tag");
const stepTwoContent = document.querySelector("#step-two-content");

const stepThreeTag = document.querySelector("#step-three-tag");
const stepThreeContent = document.querySelector("#step-three-content");

const stepTwoLine = document.querySelector(".line-2");
const stepThreeLine = document.querySelector(".line-3");

const activeTagColor = "background-color:rgba(203,223,189,1)";
const inactiveTagColor = "background-color:rgba(246,244,210,1)";

stepOneTag.addEventListener("click", (event) => {
  if ((stepOneContent.style.display = "none")) {
    stepOneContent.style.display = "block";
    stepTwoContent.style.display = "none";
    stepThreeContent.style.display = "none";
    stepOneTag.style = activeTagColor;
    stepTwoTag.style = inactiveTagColor;
    stepThreeTag.style = inactiveTagColor;
    stepTwoLine.style = inactiveTagColor;
    stepThreeLine.style = activeTagColor;
  }
});

stepTwoTag.addEventListener("click", (event) => {
  if ((stepTwoContent.style.display = "none")) {
    stepTwoContent.style.display = "block";
    stepOneContent.style.display = "none";
    stepThreeContent.style.display = "none";
    stepTwoTag.style = activeTagColor;
    stepOneTag.style = activeTagColor;
    stepThreeTag.style = inactiveTagColor;
    stepTwoLine.style = activeTagColor;
    stepThreeLine.style = inactiveTagColor;
  }
});

stepThreeTag.addEventListener("click", (event) => {
  if ((stepThreeContent.style.display = "none")) {
    stepThreeContent.style.display = "block";
    stepOneContent.style.display = "none";
    stepTwoContent.style.display = "none";
    stepThreeTag.style = activeTagColor;
    stepOneTag.style = activeTagColor;
    stepTwoTag.style = activeTagColor;
    stepTwoLine.style = activeTagColor;
    stepThreeLine.style = activeTagColor;
  }
});
document.getElementById("step-one-tag").click();


//step1.js

main();

async function main() {
  await displayAllCampsite();
  await getUserID();
  await stepThreeCreateNewEvents();
}


//Step one redirect to step two after select the e information

document
  .getElementById("step1-event-filter")
  .addEventListener("submit", async function (e) {
    e.preventDefault();
    const stepOneForm = this;
    stepOneParticipant = stepOneForm["quantity"].value;
    stepOneExperience = stepOneForm["experience"].value;
    stepOneStartDate = stepOneForm["start-date"].value;
    stepOneEndDate = stepOneForm["end-date"].value;

    if (stepOneEndDate > stepOneStartDate) {
      console.log("greater than");
      if ((stepTwoContent.style.display = "none")) {
        stepTwoContent.style.display = "block";
        stepOneContent.style.display = "none";
        stepThreeContent.style.display = "none";
        ableStep2()
      }
      await filterCampsite(stepOneExperience);
    }
  });

const step2Hover = document.querySelector("#step-two-tag");
const step3Hover = document.querySelector("#step-three-tag");



step2Hover.disabled = true
step3Hover.disabled = true

function ableStep2() {
  step2Hover.disabled = false
  console.log('ableStep2')
  stepTwoContent.style.display = "block";
  stepOneContent.style.display = "none";
  stepThreeContent.style.display = "none";
  stepTwoTag.style = activeTagColor;
  stepOneTag.style = activeTagColor;
  stepThreeTag.style = inactiveTagColor;
  stepTwoLine.style = activeTagColor;
  stepThreeLine.style = inactiveTagColor;
}

function ableStep3() {
  step3Hover.disabled = false
  console.log('ableStep3')
  stepThreeContent.style.display = "block";
  stepOneContent.style.display = "none";
  stepTwoContent.style.display = "none";
  stepThreeTag.style = activeTagColor;
  stepOneTag.style = activeTagColor;
  stepTwoTag.style = activeTagColor;
  stepTwoLine.style = activeTagColor;
  stepThreeLine.style = activeTagColor;
}


const today = new Date()

const createEventContents = document.querySelectorAll(".createAccountContent4");
const startDate = document.querySelector(".startDate");
const endDate = document.querySelector(".endDate");
const warningSign = document.querySelector(".warningSign");

endDate.addEventListener("change", function (e) {
  e.preventDefault();

  let startDT = new Date(startDate.value)
  console.log(startDT)
  console.log(today)

  for (const createEventContent of createEventContents) {
    if (startDate.value > endDate.value || startDT < today) {
      warningSign.innerHTML = "Invalid Date";
      createEventContent.classList.add("warning");
    } else {
      createEventContent.classList.remove("warning")
      warningSign.innerHTML = ""
    }
  }
});


//Step two redirect to step three after select the camping location

document
  .getElementById("step2-event-filter")
  .addEventListener("submit", async function (e) {
    e.preventDefault();
    const stepTwoForm = this;
    const stepTwoLocation = stepTwoForm["location-choice"].value;

    if ((stepThreeContent.style.display = "none")) {
      stepThreeContent.style.display = "block";
      stepOneContent.style.display = "none";
      stepTwoContent.style.display = "none";
    }
    const confirmParticipant = document.getElementById("confirm-participant");
    const confirmLocation = document.getElementById("confirm-location");
    const confirmStartDate = document.getElementById("confirm-start-date");
    const confirmEndDate = document.getElementById("confirm-end-date");
    confirmParticipant.value = stepOneParticipant;
    confirmLocation.value = stepTwoLocation;
    confirmStartDate.value = stepOneStartDate;
    confirmEndDate.value = stepOneEndDate;
    ableStep3()
  });

async function filterCampsite(userExperience) {
  const res = await fetch(`/campsite/filter/${userExperience}`);
  const resultData = await res.json();
  let descriptionHtmlStr = "";
  for (let data of resultData) {
    const description = data["description"];
    const CampsiteName = data["campsite_name"];
    descriptionHtmlStr += `
      <div class="introductionBox">
          <div class="introPics">${CampsiteName}</div>            
          <div class="wordIntro">${description}</div>   
      </div>
      `;
  }
  console.log(descriptionHtmlStr);
  const campsiteInfo = document.querySelector("#campsiteInfo");
  campsiteInfo.innerHTML = descriptionHtmlStr;
}

async function displayAllCampsite() {
  const res = await fetch("/campsite");
  const campsiteData = await res.json();
  let campsiteHtmlStr = '<option value="">請選擇你的營地</option>';
  for (let campsite of campsiteData) {
    const campsiteOption = campsite["campsite_name"];
    campsiteHtmlStr += `
        <option value="${campsiteOption}">${campsiteOption}</option>
        `;
  }
  const campsiteLocation = document.querySelector("#location-choice");
  campsiteLocation.innerHTML = campsiteHtmlStr;
}



//Get user information routers: userRoutes
async function getUserID() {
  const res = await fetch("/getUserInformation");
  const userInformation = await res.json();
  return userInformation["userId"];
}
//Step three create camping event
async function stepThreeCreateNewEvents() {
  document
    .getElementById("step3-event-filter")
    .addEventListener("submit", async function (e) {
      e.preventDefault();
      const eventForm = this;
      const participant = eventForm["confirm-participant"].value;
      const location = eventForm["confirm-location"].value;
      const startDate = eventForm["confirm-start-date"].value;
      const endDate = eventForm["confirm-end-date"].value;
      const userId = await getUserID();
      const eventFormObject = {
        participant,
        location,
        startDate,
        endDate,
        userId,
      };

      const res = await fetch("/events", {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(eventFormObject),
      });
      if (res.status === 200) {
        window.location.assign("http://localhost:8080/html/editEvent.html");
        console.log("success");
      } else {
        const data = await res.json();
        const errMessage = data.message;
        alert(errMessage);
      }
    });
}




const weatherForecast = document.querySelector(".weatherForecast");

weatherForecast.addEventListener("click", function (e) {
  e.preventDefault();
  window.location.assign("http://localhost:8080/html/ProtectedweatherForecast.html");
});

const eventManagementPage = document.querySelector(".eventMan");

eventManagementPage.addEventListener("click", function (e) {
  e.preventDefault();
  window.location.assign("http://localhost:8080/html/eventMan.html");
});

