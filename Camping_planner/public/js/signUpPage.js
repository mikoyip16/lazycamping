

const switchBox = document.querySelector('.plannerContent')
const switchBox1 = document.querySelector('.plannerContent1')

const alreadyUsingSignIn = document.querySelector('.alreadyUsingSignIn')

alreadyUsingSignIn.addEventListener('click',function(e){
    e.preventDefault
    console.log('click alreadyUsingSignIn')
    switchBox.classList.toggle('inactive')
    switchBox1.classList.toggle('inactive')
})


const dontHaveAccountSignUp = document.querySelector('.dontHaveAccountSignUp')

dontHaveAccountSignUp.addEventListener('click',function(e){
    e.preventDefault()
    switchBox.classList.toggle('inactive')
    switchBox1.classList.toggle('inactive')
})


// // submit signUp form
const createAccountForm = document.querySelector('#createAccount')

createAccountForm.addEventListener('submit', async function(e){
    e.preventDefault()
    console.log('form submitted')
    const form = this
    console.log(form)
    const username = form['username'].value
    const email = form['email'].value
    const password = form['password'].value
    const reConfirmPassword = form['password1'].value
    const formObject = { username, email, password }

  
    const res = await fetch('/signUp',{
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(formObject),
    }) 
    if (res.status = 200){
        window.location.assign('http://localhost:8080/html/stepper.html')
        alert('create account success')
    } else {
        const data = await res.json()
        const errMessage = data.message
        alert(errMessage)
    }
})


// Sign In Account

const signIn = document.querySelector('#signInForm')

console.log(signIn)

signIn.addEventListener('submit', async function(e){
    e.preventDefault()
    const form = this;
    const email = form['email'].value;
    const password = form['password'].value;
    const formObject = { email, password };
    const res = await fetch('/login',{
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(formObject),
    });
    if (res.status === 200){
        window.location.assign('http://localhost:8080/html/stepper.html')
    } else {
        const data = await res.json();
        const errMessage = data.message;
        alert(errMessage)
    }
});


const weatherForecast = document.querySelector('.weatherForecast')

weatherForecast.addEventListener('click',function(e){
    e.preventDefault()
    window.location.assign('http://localhost:8080/html/weatherForecast.html')
})


const homepage = document.querySelector('.homePage')

homepage.addEventListener('click',function(e){
    e.preventDefault()
    window.location.assign('http://localhost:8080/html/index.html')
})
//以上是dav bar


