const weatherContentBox = document.querySelector('.weather')




var current = new Date()

main()

async function main(){
    await weatherForecast()
}

async function weatherForecast(){
    const res = await fetch("https://data.weather.gov.hk/weatherAPI/opendata/weather.php?dataType=fnd&lang=tc")

    const data = await res.json();

    console.log(data)

    let htmlStr = ``

    for(let i = 0; i < 9; i++){
        htmlStr += 
        `<div class='weatherBox'>
        <img src=${weatherIcon(i)}>
        <div class="weatherItems">${data.weatherForecast[i].forecastDate.slice(4,6)}月
        ${data.weatherForecast[i].forecastDate.slice(6)}日</div>
        <div class="weatherDisplayBox weatherItems">
        <div class="weatherItems">最高氣溫：${data.weatherForecast[i].forecastMaxtemp.value}度</div>
        <div class="weatherItems">最低氣溫：${data.weatherForecast[i].forecastMintemp.value}度</div>
        </div>
        <div class="weatherItems weatherItems">天氣預告：${data.weatherForecast[i].forecastWeather}</div>
        <div class="weatherItems weatherItems">風速：${data.weatherForecast[0].forecastWind}</div>
        <br>
        </div>`
    }

    console.log(data.weatherForecast)

    function weatherIcon(i){
        if(data.weatherForecast[i].forecastWeather.includes("大致多雲，有幾陣驟雨及雷暴") || data.weatherForecast[i].forecastWeather.includes("大致多雲，間中有驟雨及幾陣雷暴")){
            return "../images/weather/rainny.png"
        } 

        if(data.weatherForecast[i].forecastWeather.includes("大致多雲，有幾陣驟雨")){
            return "../images/weather/rainny.png"
        }
        if (data.weatherForecast[i].forecastWeather.includes("大致多雲，間中有驟雨及雷暴")) {
            return "../images/weather/rainny1.png"
        }  
        if (data.weatherForecast[i].forecastWeather.includes("短暫時間有陽光，有幾陣驟雨")) {
            return "../images/weather/sunnyNrainny1.png"
        } else {
            console.log('error')
        }
    }

    weatherContentBox.innerHTML = htmlStr
}

const mainPicture = document.querySelector('.mainPics')
console.log(mainPicture)


const signUp = document.querySelector('.signUp')

signUp.addEventListener('click',function(e){
    e.preventDefault()
    window.location.assign('http://localhost:8080/html/signUpPage.html')
})


const eventManagementPage = document.querySelector('.eventMan')

eventManagementPage.addEventListener('click',function(e){
    e.preventDefault()
    window.location.assign('http://localhost:8080/html/eventMan.html')
})

const homepage = document.querySelector('.homePage')

homepage.addEventListener('click',function(e){
    e.preventDefault()
    window.location.assign('http://localhost:8080/html/index.html')
})
