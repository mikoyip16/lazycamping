export interface Event{
    id: number;
    campsiteId: number;
    startDate: Date;
    endDate: Date;
    checklistId?: number;   
}