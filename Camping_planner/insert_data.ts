import { Client } from "pg";
import xlsx from "xlsx";
import dotenv from "dotenv";
import { User, Facilities, Campsite, FoodRecommendation, EventDetail,  EquipmentCategory, 
         DefaultActivities, EquipmentJoiningEvents, UsersCreateActivities } from "./database_model";

dotenv.config();

async function main() {
    const client = new Client({
        database: process.env.DB_NAME,
        user: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
    });

    try {
        const workbook = xlsx.readFile("./databases/databases.xlsx");
        const user = workbook.Sheets["users"];
        const userJson = xlsx.utils.sheet_to_json<User>(user);
        const facilities = workbook.Sheets["facilities"];
        const facilitiesJson = xlsx.utils.sheet_to_json<Facilities>(facilities);
        const campsites = workbook.Sheets["campsite"];
        const campsitesJson = xlsx.utils.sheet_to_json<Campsite>(campsites);
        const foodRecommendation = workbook.Sheets["food_recommendation"];
        const foodRecommendationJson = xlsx.utils.sheet_to_json<FoodRecommendation>(foodRecommendation);
        const eventDetail = workbook.Sheets["event_detail"];
        const eventDetailJson = xlsx.utils.sheet_to_json<EventDetail>(eventDetail);
        const equipmentCategory = workbook.Sheets["facilities"];
        const equipmentCategoryJson = xlsx.utils.sheet_to_json<EquipmentCategory>(equipmentCategory);
        const defaultActivities = workbook.Sheets["default_activities"];
        const defaultActivitiesJson = xlsx.utils.sheet_to_json<DefaultActivities>(defaultActivities);
        const equipmentJoiningEvents = workbook.Sheets["equipment_joining_events"];
        const equipmentJoiningEventsJson = xlsx.utils.sheet_to_json<EquipmentJoiningEvents>(equipmentJoiningEvents);
        const usersCreateActivities = workbook.Sheets["user_create_activities"];
        const userCreateActivitiesJson = xlsx.utils.sheet_to_json<UsersCreateActivities>(usersCreateActivities);

        
        await client.connect();

        for (const user of userJson) {
           await client.query("INSERT INTO users (username, email, password, experience, event_id) SELECT $1, $2, $3, $4, $5 WHERE NOT EXISTS (SELECT 1 FROM users WHERE email = $6)", [
               user.username, 
               user.email, 
               user.password, 
               user.experience, 
               user.event_id,
               user.email
            ])
        }
        for (const facility of facilitiesJson) {
            await client.query("INSERT INTO facilities (name, is_hygienic) SELECT $1, $2, WHERE NOT EXISTS (SELECT 1 FROM facilities WHERE name = $3)", [
                facility.name,
                facility.is_hygienic,
                facility.name
             ])
         }
         for (const campsite of campsitesJson) {
            await client.query("INSERT INTO campsite (campsite_name, location, campsite_category, accessibility, description, facility_id, source_of_water, highlights, how_to_get_there, remarks, image, latitude, longitude) SELECT $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13 WHERE NOT EXISTS (SELECT 1 FROM campsite WHERE campsite_name = $14)", [
                campsite.campsite_name,
                campsite.location,
                campsite.campsite_category,
                campsite.accessibility,
                campsite.description,
                campsite.facility_id,
                campsite.source_of_water,
                campsite.highlights,
                campsite.how_to_get_there,
                campsite.remarks,
                campsite.image,
                campsite.latitude,
                campsite.longitude,
                campsite.campsite_name

             ])
         }
         for (const food of foodRecommendationJson) {
            await client.query("INSERT INTO food_recommendation (image, description, difficulty, step, ingredient, seasoning) SELECT $1, $2, $3, $4, $5, $6 WHERE NOT EXISTS (SELECT 1 FROM food_recommendation WHERE description = $7)", [
                food.image,
                food.description,
                food.difficulty,
                food.step,
                food.ingredient,
                food.seasoning,
                food.description,
             ])
         }
         for (const event of eventDetailJson) {
            await client.query("INSERT INTO event_detail (campsite_id, start_date, end_date, weather, checklist_id) VALUES ($1, $2, $3, $4, $5)", [
                event.campsite_id,
                event.start_date,
                event.end_date,
                event.weather,
                event.checklist_id,
             ])
         }
         for (const equipmentCat of equipmentCategoryJson) {
            await client.query("INSERT INTO equipment_category (category_name) SELECT $1 WHERE NOT EXISTS (SELECT 1 FROM equipment_category WHERE category_name = $2)", [
                equipmentCat.category_name,
                equipmentCat.category_name
             ])
         }
         for (const defaultActivity of defaultActivitiesJson) {
            await client.query("INSERT INTO default_activities (remark, activity_name, equipment_id) SELECT $1, $2, $3 WHERE NOT EXISTS (SELECT 1 FROM users_create_activities WHERE activity_name = $4)", [
                defaultActivity.remark,
                defaultActivity.activity_name,
                defaultActivity.equipment_id,
                defaultActivity.activity_name
             ])
         }
         for (const equipmentJoinEvent of equipmentJoiningEventsJson) {
            await client.query("INSERT INTO equipment_joining_events (equipment_id, event_id, remark, is_prepared) SELECT $1, $2, $3, $4 WHERE NOT EXISTS (SELECT 2 FROM equipment_joining_events WHERE equipment_id = $5 and event_id = $6)", [
                equipmentJoinEvent.equipment_id,
                equipmentJoinEvent.event_id,
                equipmentJoinEvent.remark,
                equipmentJoinEvent.is_prepared,
                equipmentJoinEvent.equipment_id,
                equipmentJoinEvent.event_id
             ])
         }
         for (const newActivity of userCreateActivitiesJson){
            await client.query("INSERT INTO users_create_activities (start_date, end_date, activity_name, remark, event_id, default_id, is_default) VALUES ($1, $2, $3, $4, $5, $6, $7)", [
                newActivity.start_date,
                newActivity.end_date,
                newActivity.activity_name,
                newActivity.remark,
                newActivity.event_id,
                newActivity.default_id,
                newActivity.is_default,
                newActivity.start_date,
                newActivity.end_date,
                newActivity.activity_name
             ])
         }

        
             
         
    } catch (err) {
        console.error(err.message);
    }
    // finally {
    // }
    await client.end();
}

main();