export interface User {
    username: string;
    email: string;
    password: string;
    experience?: string;
    event_id?: number;
};

export interface Facilities {
    name: string;
    is_hygienic: boolean;
};

export interface Campsite {
    campsite_name: string;
    location: string;
    campsite_category: string;
    accessibility: string;
    description: string;
    facility_id:number;
    source_of_water: string;
    highlights: string;
    how_to_get_there: string;
    remarks: string;
    image: string;
    latitude: number;
    longitude: number;
};

export interface FoodRecommendation {
    image: string;
    description: string;
    difficulty: string;
    step: string;
    ingredient: string;
    seasoning: string;
};

export interface EventDetail {
    campsite_id: string;
    start_date: Date;
    end_date: Date;
    weather: string;
    checklist_id: number;
};

export interface EquipmentCategory {
    category_name: string;
};

export interface DefaultActivities {
    remark:string;
    activity_name: string;
    equipment_id: number;
};

export interface UsersCreateActivities {
    start_date: Date;
    end_date: Date;
    activity_name: string;
    remark: string;
    event_id: number;
    default_id: number;
    is_default: boolean
}

export interface EquipmentJoiningEvents {
    equipment_id: number;
    event_id: number;
    remark: string;
    is_prepared: boolean;
}