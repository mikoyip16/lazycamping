import express from "express";
import expressSession from "express-session";
import path from "path";
import { Client } from "pg";
import dotenv from "dotenv";
import http from "http";
import { isLoggedIn } from "./guard"
import { Server as SocketIO } from "socket.io";


dotenv.config();

export const client = new Client({
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
});

client.connect();

const app = express();
const server = new http.Server(app);
export const io = new SocketIO(server);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(
    expressSession({
        secret: "Group One is the Best",
        resave: true,
        saveUninitialized: true,
    })
);

import { userRoutes } from "./routers/userRoutes";
import { activityRoutes } from "./routers/activityRoutes";

app.use('/', userRoutes)


app.use(express.static(path.join(__dirname, "public")));
app.use(isLoggedIn, express.static(path.join(__dirname, "protected")))


import { eventRoutes } from "./routers/eventRoutes";
import { campsiteRoutes } from "./routers/campsiteRoutes";
import { eventManRoutes } from "./routers/eventManRoutes";
app.use("/events", eventRoutes)
app.use("/campsite", campsiteRoutes)
app.use("/activity", activityRoutes)
app.use("/eventManagement", eventManRoutes)



const PORT = 8080;
server.listen(PORT, () => {
    console.log(`listening to port [${PORT}]`);
});


